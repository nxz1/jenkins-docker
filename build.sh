echo "Building the jenkins Image"
sleep 1;
docker build -t jenkins-lts-image --rm=true --no-cache=true --force-rm .
echo ""
echo "Jenkins image built"
sleep 1;
echo "Run start.sh script to deploy your jenkins container"
