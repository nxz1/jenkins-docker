# Jenkins LTS as base
FROM jenkins/jenkins:lts

# Labels
LABEL env="development"
LABEL Maintainer="Mohammed Nawaz<nawaz.nxztech@gmail.com>"

# Working directory
WORKDIR /var/jenkins_home

# Some arguements
ARG PORT1=9090 
ARG PORT2=50000 

# Becoming root to administer
USER root

# Installing required packages
RUN apt-get update && apt-get install apt-utils -y && \
    apt-get upgrade -y && \
    apt-get install git maven curl vim wget -y

# Setting up maven home
ENV MAVEN_HOME=/usr/share/maven

# Switching back to jenkins user
USER jenkins

# Exposing ports
EXPOSE $PORT1
EXPOSE $PORT2
