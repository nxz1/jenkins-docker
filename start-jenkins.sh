echo -e ""
echo -e "Press enter for help"
echo -e ""
read -p "Are you sure, you want to continue with default volume ? (y/n) " CONFIRMATION
echo ""

if [[ $CONFIRMATION == "y" || $CONFIRMATION == "Y" ]]
then
	echo "Creating jenkins container"
	sleep 1;
	[ -d ./jenkins_data ] && echo "jenkins_data folder already exist" || mkdir ./jenkins_data && echo "jenkins_data binded to JENKINS_HOME"
	sleep 1;
	echo ""
	echo "Deploying jenkins container"
	sleep 1;
	# IMPORTANT: Replace host directory path for volume before running !
	docker run -d --name jenkins -p 9090:9090 -p 50000:50000 -v ./jenkins_data:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock -e JENKINS_OPTS="--httpPort=9090" jenkins-lts-image | xargs echo "container-id: "
	sleep 1 && echo "Jenkins container is now running on port 9090"
elif [[ $CONFIRMATION == "n" || $CONFIRMATION == "N" ]]
then
	echo -e "Change default Volume"
	echo -e ""
	echo -e "update line 17 of start.sh script as per the host machine"
	echo -e "\t hint:   -v /path/to/your/directory:/var/jenkins_home "
else
	echo "By default ./jenkins_data will be created"
	echo -e "You can change this in line 17 of this i.e start.sh script"
	echo -e ""
	echo -e "\ty -> Continue with default bind volume"
	echo -e "\tn -> how to change default volume"
fi
