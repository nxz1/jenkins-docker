# User Input
read -p "Do you want to remove the old jenkins data (y/N)?" AGREE
#echo $AGREE

# Logic
if [[ $AGREE == "y" || $AGREE == "Y" ]]
then
	echo "Deleting old data";
	sleep 1; rm -rf ./jenkins_data
	sleep 1; echo "Deleted old jenkins data";
	echo ""
	sleep 1; docker rm -f ci | xargs echo "deleted container: "

elif [[ $AGREE == "n" || $AGREE == "N" ]]
then
	echo "Keeping old data";
	sleep 1; docker rm -f ci | xargs echo "deleted container: "
else
	echo "Keeping old data";
	sleep 1; docker rm -f ci | xargs echo "deleted: "
fi
